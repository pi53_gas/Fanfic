<?php
class Bootstrap{
    function __construct()
    {
        if (isset($_GET['url']) && !empty($_GET['url']))
        {
            $url = $_GET['url'];
            $url = rtrim($url, '/');
            $url = explode('/', $url);
            $controller = ucfirst(array_shift($url));
            $method = ucfirst(array_shift($url)) . 'Action';
            $parameter = ucfirst(array_shift($url));
            $file = 'controllers/' . $controller . '.php';
            if(file_exists($file))
                require $file;
            $main = new $controller;
            if (isset($parameter)) {
                $main->$method($parameter);
            } else {
                if (isset($method))
                    $main->$method();
            }
        }
    }
}