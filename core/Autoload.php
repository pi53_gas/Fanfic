<?php
function __autoload($className){
    $path = array(
        'models/',
        'controllers/',
        'core/'
    );
    foreach ($path as $route)
    {
        $route = $route . $className . '.php';
        if(is_file($route))
            include_once ($route);
    }
}