<?php
class Route{
    public function Init(){

    }
    public function Run()
    {
        if(isset($_GET['url']) && !empty($_GET['url'])){
            $url = $_GET['url'];
            $url = rtrim($url, '/');
            $url = explode('/', $url);

            $controllerName = ucfirst(array_shift($url)).'Controller';
            $methodName = array_shift($url).'Action';

            $options = $url;
            $file = 'controllers/' . $controllerName . '.php';
            if(file_exists($file))
                include_once ($file);
        }
        else{
            $controllerName = 'MainController';
            $methodName = 'indexAction';
        }
        if(class_exists($controllerName)){
            $controller = new $controllerName;
            if(method_exists($controller, $methodName)) {
                if (isset($options)){
                    //$controller->$methodName($options);
                    call_user_func_array(array($controller, $methodName), $options);
                }
                else {
                    $controller->$methodName();
                }
            }
            else {
                require 'controllers/ErrorController.php';
                $error = new ErrorController();
            }
        }
        else {
            require 'controllers/ErrorController.php';
            $error = new ErrorController();
        }
    }
}