<?php
require ('libs/rb.php');

class Database{
    protected $connect;

    public function __construct($host, $database_name, $user, $password)
    {
        R::setup("mysql:host={$host};dbname={$database_name}", $user, $password);
        R::freeze(false);
        if(!R::testConnection()){
            exit("Нет подключения к БД");
            //Написать логи
        }
        //$this->connect = new PDO("mysql:host={$host};dbname={$database_name}", $user, $password);
    }

    public function readOneData($table, $id)
    {
        $sql = R::load($table, $id);
        return $sql;
    }
    public function readData($table)
    {
        $sql = "SELECT * FROM `{$table}`";
        $rows= R::getAll($sql);
        return $rows;
    }
    public function readInformation($columns, $table){
        $sql = "SELECT {$columns} FROM `{$table}` 
        WHERE `status` = 1
        ORDER BY `id`";
        $rows= R::getAll($sql);
        return $rows;
    }
    public function readSomeData($columns, $table, $limit, $offset, $category, $id)
    {
        $sql = "SELECT {$columns} FROM `{$table}` 
        WHERE `status` = 1 and {$category} = {$id}
        ORDER BY `id`
        LIMIT {$limit}
        OFFSET {$offset}";
        $rows= R::getAll($sql);
        return $rows;
    }
    public function readDataById($columns, $table, $limit, $category, $id){
        $sql = "SELECT {$columns} FROM `{$table}` 
        WHERE `status` = 1 and {$category} = {$id}
        ORDER BY `id`
        LIMIT {$limit}";
        $rows= R::getAll($sql);
        return $rows;
    }
    public function getCount($table, $category, $id){
        $count = R::count($table, "{$category} = ?",  [$id]);
        return $count;
    }
    public function find($table, $field, $condition)
    {
        $sql = R::find($table, "{$field}", array($condition));
        return $sql;
    }
    public function insertData($table, $data)
    {
        $fieldArray = array_keys($data);
        $valuesArray = array_values($data);
        $fields = implode(', ', $fieldArray);
        $values = "'".implode("', '", $valuesArray)."'";
        R::exec( "INSERT INTO {$table} {$fields} VALUES ({$values})");
    }
    public function deleteData($table, $option, $data){
        R::exec("DELETE FROM {$table} WHERE {$option}=?", array($data));
    }
}