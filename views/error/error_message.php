<div class='alert alert-danger alert-dismissible fade in' role='alert'>
    <button aria-label='Close' class='close' data-dismiss='alert' type='button'>
        <span aria-hidden='true'>&#215;</span>
    </button>
    <h4>Oh snap! You got an error!</h4>
    <p>
        Sorry, the page you are looking for can not be found!Possibility of the page has been deleted, or you misspell a URL.
    </p>
    <p>
        <button class='btn btn-danger' type='button'>
            <a href='http://phrozen2-bt.blogspot.com/' title='Sakura Blogger Template'>
                Back To Main Page
            </a>
        </button>
    </p>
</div>