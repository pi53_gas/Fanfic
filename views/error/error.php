<?php require 'views/layouts/header/links.php'; ?>
<body>
<div>
    <?php require 'views/layouts/header/banner.php'; ?>
    <div class='container' id='outer-wrapper'>
        <div class='row fx' id='top-wrapper'>
            <?php require 'views/layouts/header/header.php'; ?>
            <?php require 'views/layouts/header/navigation.php'; ?>
            <div class='row fx' id='content-wrapper'>
                <?php require 'error_message.php'; ?>
            </div>
            <div class='clear'></div>
        </div>
    </div>
    <?php require 'views/layouts/footer.php'; ?>
    <div class='clear'></div>
</div>
<script type="text/javascript" src="https://www.blogger.com/static/v1/widgets/2388068295-widgets.js"></script>
</body>
</html>