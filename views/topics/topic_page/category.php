<h1>Topics</h1>

<div class='widget-content'>
    <div class="wfeet-posts row">
        <?php foreach ($theme as $topic): ?>
        <article class="wfeet-item post hentry col-xs-6 col-sm-6 col-md-4">
            <div class="wfeet_thumbs">
                <a href="/category/<?php echo $topic['id']; ?>">
                    <img src="/templates/images/topics/<?php echo $topic['image']; ?>" style="height: 288px;">
                    <span class="ftime wfeet_title">
                        <?php echo $topic['name']; ?>
                    </span>
                </a>
            </div>
        </article>
        <?endforeach; ?>
    </div>
</div>

