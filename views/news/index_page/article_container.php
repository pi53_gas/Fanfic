<b class='main-title'><i class='glyphicon glyphicon-menu-hamburger'></i> Recent Anime</b>
<div class='main section' id='main' name='Main Blog'>
    <div class='widget Blog' data-version='1' id='Blog1'>
        <div class='blog-posts hfeed'>
            <?php require 'article.php'; ?>
            <div class='clear'></div>
        </div>
        <nav class='pagination blog-pager' id='blog-pager'>
            <ul class='pagination'></ul>
            <div class='clear'></div>
        </nav>
        <div class='clear'></div>
    </div>
</div>