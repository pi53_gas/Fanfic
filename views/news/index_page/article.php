<!-- С Т А Т Ь Я-->
<?php foreach ($newsList as $news): ?>
    <article class='post hentry'>
        <div class='thumb_post'>
            <img alt="IMAGE" src="/templates/images/news/article/<?php echo $news['image']; ?>" width="200px">
        </div>
        <div class='main_post'>
            <h2 class='post-title entry-title'>
                <a href='#' title='Chuunibyou demo Koi ga Shitai!'>
                    <?php echo $news['title']; ?>
                </a>
            </h2>
            <header class='post-header fx'>
            <span class='fuser'>
                <i class='glyphicon glyphicon-user'></i>
                <span class='fn'>
                    <span class='given-name'> <?php echo $news['author']; ?></span>
                </span>
            </span>
                <span class='ftime'>
                <i class='glyphicon glyphicon-time'></i>
                <a class='updated' href='#' rel='bookmark' title='permanent link'>
                    <abbr class='published' title='2014-12-18T00:07:00-08:00'>
                        <?php echo $news['date']; ?>
                    </abbr>
                </a>
            </span>
                <span class='fcoment'>
                <i class='glyphicon glyphicon-comment'></i>
                <a class='comment-link' href='#' onclick=''>
                    11 comments
                </a>
            </span>
            </header>
            <div class='post-body entry-content fx' id='post-body-6722705426382019935'>
                <p><?php echo $news['preview']; ?></p>
            </div>
            <footer class='post-footer fx'>
                <span class='fl post-share'>
                    <a class='goog-inline-block share-button sb-email' href='http://www.blogger.com/share-post.g?blogID=8043304422620558613&amp;postID=6722705426382019935&amp;target=email' target='_blank' title='Email This'>
                        <span class='share-button-link-text'>Email This</span>
                    </a>
                    <a class='goog-inline-block share-button sb-blog' href='http://www.blogger.com/share-post.g?blogID=8043304422620558613&amp;postID=6722705426382019935&amp;target=blog' onclick='window.open(this.href, "_blank", "height=270,width=475"); return false;' target='_blank' title='BlogThis!'>
                        <span class='share-button-link-text'>BlogThis!</span>
                    </a>
                    <a class='goog-inline-block share-button sb-twitter' href='http://www.blogger.com/share-post.g?blogID=8043304422620558613&amp;postID=6722705426382019935&amp;target=twitter' target='_blank' title='Share to Twitter'>
                        <span class='share-button-link-text'>
                            Share to Twitter
                        </span>
                    </a>
                    <a class='goog-inline-block share-button sb-facebook' href='http://www.blogger.com/share-post.g?blogID=8043304422620558613&amp;postID=6722705426382019935&amp;target=facebook' onclick='window.open(this.href, "_blank", "height=430,width=640"); return false;' target='_blank' title='Share to Facebook'>
                        <span class='share-button-link-text'>
                            Share to Facebook
                        </span>
                    </a>
                    <div class='goog-inline-block dummy-container'>
                        <div class='g-plusone' data-size='medium'></div>
                    </div>
                </span>
                <span class='fr post-read'>
                    <a href='/news/view/<?php echo $news['id']; ?>' title='Chuunibyou demo Koi ga Shitai!'>Continue
                    <i class='glyphicon glyphicon-menu-right'></i>
                    </a>
                </span>
            </footer>
            <div class='clear'></div>
        </div>
        <div class='clear'></div>
    </article>
<?php endforeach; ?>