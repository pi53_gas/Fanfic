<div class='post-body entry-content fx' id='post-body-6722705426382019935' itemprop='description articleBody'>
    <div class='synop_top'>
        <div class='synop_thumbs'>
<!--            Путь к картинке-->
            <a href="#" imageanchor="1" >
                <img border="0" src="/templates/images/news/article/<?php echo $newsItem['image']; ?>" />
            </a>
        </div>
        <ul class='synop_infs'>
            <li>
                <b>
                    English:
                </b>
                <span>
                    Steins;Gate
                </span>
            </li>
            <li>
                <b>
                    Japanese:
                </b>
                <span>
                    シュタインズ ゲート
                </span>
            </li>
            <br/>
            <li>
                <b>
                    Type:
                </b>
                <span>
                    TV
                </span>
            </li>
            <li>
                <b>
                    Episodes:
                </b>
                <span>
                    24
                </span>
            </li>
            <li>
                <b>
                    Status:
                </b>
                <span>
                    Finished Airing
                </span>
            </li>
            <li>
                <b>
                    Aired:
                </b>
                <span>
                    Apr 6, 2011 to Sep 14, 2011
                </span>
            </li>
            <li>
                <b>
                    Producers:
                </b>
                <span>
                    Frontier Works, FUNimation EntertainmentL, Media Factory, Movic, AT-X, White Fox, Kadokawa Pictures Japan, Nitroplus
                </span>
            </li>
            <li>
                <b>
                    Genres:
                </b>
                <span>
                    Sci-Fi, Thriller
                </span>
            </li>
            <li>
                <b>
                    Duration:
                </b>
                <span>
                    24 min. per episode
                </span>
            </li>
        </ul>
    </div>
    <?php require 'content.php'; ?>
</div>