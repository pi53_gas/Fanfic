<h1 class='post-title entry-title' itemprop='headline'>
    <?php echo $newsItem['title']; ?>
</h1>
<header class='post-header'>
    <span class='fuser vcard'>
        <i class='glyphicon glyphicon-user'></i>
        <span class='fn' itemprop='author' itemscope='itemscope'>
            <span itemprop='name'>
                <?php echo $newsItem['author']; ?>
            </span>
        </span>
    </span>
    <span class='ftime'>
        <i class='glyphicon glyphicon-time'></i>
        <abbr class='published' itemprop='datePublished dateModified' title='2014-12-18T00:07:00-08:00'>
            <?php echo $newsItem['date']; ?>
        </abbr>
    </span>
    <span class='fcom'>
        <i class='glyphicon glyphicon-comment'></i>
        11
    </span>
    <div class='clear'></div>
</header>