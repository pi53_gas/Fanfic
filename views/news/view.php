<?php require 'views/layouts/header/links.php'; ?>
<body>
<div>
    <?php require 'views/layouts/header/banner.php'; ?>
    <div class='container' id='outer-wrapper'>
        <div class='row fx' id='top-wrapper'>
            <?php require 'views/layouts/header/header.php'; ?>
            <?php require 'views/layouts/header/navigation.php'; ?>
            <div class='row fx' id='content-wrapper'>
                <div class='col-xs-12 col-sm-8 col-md-8' id='main-wrapper' role='main'>
                    <div class='main section' id='main' name='Main Blog'>
                        <div class='widget Blog' data-version='1' id='Blog1'>
                            <div class='blog-posts hfeed'>
                                <!--                    С Т А Т Ь Я-->
                                <article class='post hentry' itemprop='blogPost' itemscope='itemscope' itemtype='http://schema.org/BlogPosting'>
                                    <?php require 'views/news/view_page/path.php'; ?>
                                    <?php require 'views/news/view_page/title.php'; ?>
                                    <?php require 'views/news/view_page/information.php'; ?>
                                    <?php require 'views/news/view_page/share.php'; ?>
                                    <!--                        --><?php //require 'views/news/view_page/related_posts.php'; ?>
                                </article>

                                <!--                                C O M M E N T S-->
                                <div class='comments' id='comments'>
                                    <?php require 'views/news/view_page/comments.php'; ?>
                                </div>
                                <div class='clear'></div>
                            </div>
                            <div class='clear'></div>
                        </div>
                    </div>
                </div>
                <?php require 'views/layouts/aside/aside.php'; ?>
                <div class='clear'></div>
            </div>
        </div>
        <?php require 'views/layouts/footer.php'; ?>
        <div class='clear'></div>
    </div>
    <script type="text/javascript" src="https://www.blogger.com/static/v1/widgets/2388068295-widgets.js"></script>
</body>
</html>