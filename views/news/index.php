<?php require 'views/layouts/header/links.php'; ?>
<body>
<div>
    <?php require 'views/layouts/header/banner.php'; ?>
    <div class='container' id='outer-wrapper'>
        <div class='row fx' id='top-wrapper'>
            <?php require 'views/layouts/header/header.php'; ?>
            <?php require 'views/layouts/header/navigation.php'; ?>
        <div class='row fx' id='content-wrapper'>
            <!-- С Л А Й Д Е Р-->
            <div class='col-xs-12 col-sm-8 col-md-8' id='main-wrapper' role='main'>
                <?php require 'views/layouts/slider/slider.php'; ?>
                <?php require 'views/news/index_page/article_container.php'; ?>
                <?php require 'views/layouts/widget/widget_posts.php'; ?>
            </div>
            <?php require 'views/layouts/aside/aside.php'; ?>
            <div class='clear'></div>
        </div>
    </div>
        <?php require 'views/layouts/footer.php'; ?>
    <div class='clear'></div>
</div>
<script type="text/javascript" src="https://www.blogger.com/static/v1/widgets/2388068295-widgets.js"></script>
</body>
</html>