<?php require 'views/layouts/header/links.php'; ?>
<?php require 'links.php'; ?>
<body>
<div>
    <?php require 'views/layouts/header/banner.php'; ?>
    <div class='container' id='outer-wrapper'>
        <div class='row fx' id='top-wrapper'>
            <?php require 'views/layouts/header/header.php'; ?>
            <?php require 'views/layouts/header/navigation.php'; ?>
            <div class='row fx' id='content-wrapper'>
                <?php if(is_array($errors) && isset($errors)): ?>
                    <div>
                        <?php echo array_shift($errors)?>
                    </div>
                <?php endif; ?>
<!--                <div class='col-xs-12 col-sm-8 col-md-8' id='main-wrapper' role='main'>-->
                    <?php require 'form.php'; ?>
<!--                </div>-->
                <div class='clear'></div>
            </div>
        </div>
        <?php require 'views/layouts/footer.php'; ?>
        <div class='clear'></div>
    </div>
    <?php require 'scripts.php'; ?>
    <script type="text/javascript" src="https://www.blogger.com/static/v1/widgets/2388068295-widgets.js"></script>
    <script type='text/javascript'>
        /*POST*/
    </script>
</body>
</html>