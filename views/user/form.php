<div class="limiter">
    <img src="/templates/images/user/meme/1.png" alt="Meme" class="meme one">
    <img src="/templates/images/user/meme/2.png" alt="Meme" class="meme two">
    <img src="/templates/images/user/meme/3.png" alt="Meme" class="meme star">
    <img src="/templates/images/user/meme/4.png" alt="Meme" class="meme person">
    <img src="/templates/images/user/meme/5.png" alt="Meme" class="meme smile">
    <img src="/templates/images/user/meme/6.png" alt="Meme" class="meme pretty">
    <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
            <form class="login100-form validate-form" action="#" method="POST">
					<span class="login100-form-title p-b-55">
						Registration
					</span>
                <div class="wrap-input100 validate-input m-b-16" data-validate = "Логин не должен быть пустым">
                    <input class="input100" type="text" name="username" placeholder="Username" value="<?php echo @$data['username']; ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<span class="lnr lnr-user"></span>
						</span>
                </div>
                <div class="wrap-input100 validate-input m-b-16" data-validate = "Некорректный email">
                    <input class="input100" type="text" name="email" placeholder="Email" value="<?php echo @$data['email']; ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate = "Пароль не должен быть пустым">
                    <input class="input100" type="password" name="pass" placeholder="Password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
                </div>
                <div class="wrap-input100 validate-input m-b-16" data-validate = "Повторите пароль">
                    <input class="input100" type="password" name="repeat-pass" placeholder="Repeat Password">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
                </div>

                <div class="container-login100-form-btn p-t-25">
                    <button class="login100-form-btn" type="submit" name="sign-up">
                        Sign Up
                    </button>
                </div>


            </form>
        </div>
    </div>
</div>





