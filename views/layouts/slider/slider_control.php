<!-- Controls -->
<a class='left carousel-control' data-slide='prev' href='#carousel-generic' role='button'>
    <span aria-hidden='true' class='glyphicon glyphicon-chevron-left'></span>
    <span class='sr-only'>Previous</span>
</a>
<a class='right carousel-control' data-slide='next' href='#carousel-generic' role='button'>
    <span aria-hidden='true' class='glyphicon glyphicon-chevron-right'></span>
    <span class='sr-only'>Next</span>
</a>