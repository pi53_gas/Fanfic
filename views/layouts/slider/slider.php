<div class='carousel slide' data-ride='carousel' id='carousel-generic'>
    <!-- Indicators -->
    <ol class='carousel-indicators'>
        <li class='active' data-slide-to='0' data-target='#carousel-generic'></li>
        <li data-slide-to='1' data-target='#carousel-generic'></li>
        <li data-slide-to='2' data-target='#carousel-generic'></li>
        <li data-slide-to='3' data-target='#carousel-generic'></li>
        <li data-slide-to='4' data-target='#carousel-generic'></li>
    </ol>
    <!-- Wrapper for slides -->
    <?php require 'slider_pictures.php'; ?>
    <?php require 'slider_control.php'; ?>
</div>