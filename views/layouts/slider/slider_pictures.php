<!-- К А Р Т И Н К И  Д Л Я  С Л А Й Д Е Р А-->
<div class='carousel-inner' role='listbox'>
    <div class='item active'>
        <img alt='Holo  Kemonomimi ' src='/templates/images/news/1.png'/>
        <div class='carousel-caption'>
            <h3>Holo  Kemonomimi </h3>
            <p>Fairy Tail.</p>
        </div>
    </div>
    <div class='item'>
        <img alt='Pikachu ' src='/templates/images/news/2.jpg'/>
        <div class='carousel-caption'>
            <h3>Pikachu </h3>
            <p>Pokemon</p>
        </div>
    </div>
    <div class='item'>
        <img alt='Anime' src='/templates/images/news/3.png'/>
        <div class='carousel-caption'>
            <h3>Akagami no Shirayuki-hime 2nd Season</h3>
            <p>In the kingdom of Tanbarun lives Shirayuki, an independent and strong-willed young woman.</p>
        </div>
    </div>
    <div class='item'>
        <img alt='Shoujo-tachi wa Kouya wo Mezasu' src='/templates/images/news/4.jpg'/>
        <div class='carousel-caption'>
            <h3>Shoujo-tachi wa Kouya wo Mezasu</h3>
            <p>Buntarou doesn&#8217;t know what he wants to do in the future.</p>
        </div>
    </div>
    <div class='item'>
        <img alt='HaruChika' src='/templates/images/news/5.jpg'/>
        <div class='carousel-caption'>
            <h3>HaruChika: Haruta to Chika wa Seishun Suru</h3>
            <p>The original novels revolve around two high school students named Haruta and Chika.</p>
        </div>
    </div>
</div>