<!-- Н А В И Г А Ц И Я  П О  С А Й Т У-->
<nav class='fx' id='nav-wrapper' role='navigation'>
    <input class='fh' id='mob-put' type='checkbox'/>
    <label class='fh' id='mob-lab'><i class='glyphicon glyphicon-menu-hamburger'></i> Navigation</label>
    <ul class='nav navbar-nav fx'>
        <li>
            <a href='/main/home' itemprop='url'>
                <span itemprop='name'>
                    <i class='glyphicon glyphicon-home'></i> Home
                </span>
            </a>
        </li>
        <li>
            <a href='/news/index' itemprop='url'>
                <span itemprop='name'>
                    <i class='glyphicon glyphicon-file'></i>Blog
                </span>
            </a>
        </li>
        <li>
            <a href='/news/page' itemprop='url'>
                <span itemprop='name'>
                    <i class='glyphicon glyphicon-user'></i> Staff
                </span>
            </a>
        </li>
        <li>
            <a href='#' itemprop='url'>
                <span itemprop='name'>
                    <i class='glyphicon glyphicon-envelope'></i> Contact
                </span>
            </a>
        </li>
        <li>
            <a href='#' itemprop='url'><span itemprop='name'>
                    <i class='glyphicon glyphicon-folder-open'></i> Archive
                </span>
            </a>
        </li>
    </ul>
</nav>
</div>