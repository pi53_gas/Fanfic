<!DOCTYPE html>
<html class='v2' dir='ltr' xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <!-- Meta Tag -->
    <meta charset='utf-8'/>
    <title>Home Page</title>

    <!--И К О Н К А _ С А Й Т А-->
    <link href='/templates/icon/ninjatoy.ico' rel='icon' type='image/x-icon'/>

    <!--С Т И Л И-->
    <link crossorigin='anonymous' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' rel='stylesheet'/>

    <!--С Л А Й Д Е Р-->
    <script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
    <script crossorigin='anonymous' integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <script type='text/javascript'>
        /*Data*/
    </script>
    <script type='text/javascript'>
        /*Information*/
    </script>

    <link rel="stylesheet" href="/templates/css/page/body.css">
    <link rel="stylesheet" href="/templates/css/page/bundle.css">
    <link rel="stylesheet" href="/templates/css/page/cdata.css">
    <link rel="stylesheet" href="/templates/css/page/color.css">
    <link rel="stylesheet" href="/templates/css/page/comments.css">
    <link rel="stylesheet" href="/templates/css/page/content.css">
    <link rel="stylesheet" href="/templates/css/page/footer.css">
    <link rel="stylesheet" href="/templates/css/page/header.css">
    <link rel="stylesheet" href="/templates/css/page/layout.css">
    <link rel="stylesheet" href="/templates/css/page/naw_wrapper.css">
    <link rel="stylesheet" href="/templates/css/page/news_ticker.css">
    <link rel="stylesheet" href="/templates/css/page/other.css">
    <link rel="stylesheet" href="/templates/css/page/pager.css">
    <link rel="stylesheet" href="/templates/css/page/post_share_in_post.css">
    <link rel="stylesheet" href="/templates/css/page/responsive_add.css">
    <link rel="stylesheet" href="/templates/css/page/search.css">
    <link rel="stylesheet" href="/templates/css/page/style.css">
    <link rel="stylesheet" href="/templates/css/page/unslider.css">
    <link rel="stylesheet" href="/templates/css/page/widget.css">
    <script src="/templates/css/page/cdata.js" async></script>
    <script src="/templates/css/page/data.js" async></script>
    <link rel="stylesheet" href="/templates/css/error/error_message.css">
<!--    <script src="/templates/css/news/information.js" async></script>-->
