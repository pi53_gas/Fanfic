<h1>Category</h1>

<div class='widget-content'>
    <div class="wfeet-posts row">
        <?php foreach ($kind as $c): ?>
        <article class="wfeet-item post hentry col-xs-6 col-sm-6 col-md-4">
            <div class="wfeet_thumbs">
                <a href="/topics/topic/<?php echo $c['id']; ?>">
                    <img src="/templates/images/category/<?php echo $c['image']; ?>" style="height: 288px;">
                    <span class="ftime wfeet_title">
                        <?php echo $c['name']; ?>
                    </span>
                </a>
            </div>
        </article>
        <?endforeach; ?>
    </div>
</div>

