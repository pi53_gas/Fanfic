<?php
class TopicsController
{
    public function topicAction($topicId, $page = 1){
        $topics = new Topics();
        $theme = $topics->getTopics($topicId, $page);
        $total = $topics->getTopicsCount($topicId);
        $pagination = new Pagination($total, $page, $topics->limit, '');
        require ('views/topics/topic.php');
    }
}