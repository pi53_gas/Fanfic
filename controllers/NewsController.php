<?php
//require('models/News.php');
class NewsController extends Controller{

    public function indexAction()
    {

        $news = new News();
        $newsList = $news->getNews();
        require ('views/news/index.php');

    }
    public function viewAction($id){
        //Просмотр одной записи
        $news = new News();
        $newsItem = $news->getNewsById($id);
        require ('views/news/view.php');

    }
}