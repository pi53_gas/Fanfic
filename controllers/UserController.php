<?php
class UserController{
    private $user;
    public function __construct()
    {
       $this->user = new User();
    }

    public function registrationAction(){
        $data = $_POST;
        $errors = array();
        if(isset($data['sign-up']))
        {

            if(empty(trim($data['username'])))
            {
                $errors[] = 'Логин пустой';
            }
            if(empty(trim($data['email'])))
            {
                $errors[] = 'Email пустой';
            }
            if(empty($data['pass']))
            {
                $errors[] = 'Пароль пустой';
            }
            if($this->user->checkUsername($data['username'])){
                $errors[] = 'Логин должен быть больше 2 символов.';
            }
//            if($this->user->checkEmail($data['email'])){
//                $errors[] = 'Некорректный Email.';
//            }
            if($this->user->checkPassword($data['pass']) == false)
            {
                $errors[] = 'Пароль должен быть больше 6 символов.';
            }
            if(strcmp($data['pass'], $data['repeat-pass']) != 0)
            {
                $errors[] = 'Повторный пароль введен не верно.';
            }
            if($this->user->checkUserExists($data['username']))
            {
                $errors[] = 'Пользователь с таким логином существует.';
            }
            if($this->user->checkEmailExists($data['email']))
            {
                $errors[] = 'Пользователь с таким Email существует.';
            }
            if(empty($errors))
            {
                $this->user->registration($data['username'], $data['email'], $data['pass']);
//
//                echo '<div>Вы зарегестрировались</div>';
            }
//            else{
//                echo '<div>'.array_shift($errors).'</div>';
//            }
        }
        require_once ('views/user/registration.php');
    }
}