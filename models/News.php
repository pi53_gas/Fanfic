<?php

class News extends Model{
    //private $connect;
    public function __construct()
    {
        parent::__construct();
        //$this->connect = new Database(HOST, DATABASE_NAME, USER, PASSWORD);
    }

    public function getNewsById($id)
    {
        $news = $this->connect->readOneData('article', $id);
        return $news;
    }
    public function getNews()
    {
        $news = $this->connect->readData('article');
        return $news;
    }
}