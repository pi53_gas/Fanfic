<?php
class Topics extends Model {
    public $limit = 6;
    public function __construct()
    {
        parent::__construct();
        //$this->connect = new Database(HOST, DATABASE_NAME, USER, PASSWORD);
    }
    public function getTopics($id, $page = 1){
        $page = intval($page);
        $offset = ($page-1) * $this->limit;
        $topics = $this->connect->readSomeData('id, name, image', 'topics',  $this->limit, $offset, 'category_id', $id);
        return $topics;
    }
    public function getTopicsCount($id){
        $topic = $this->connect->getCount('topics', 'category_id', $id);
        return $topic;
    }
}
