<?php
class Category extends Model{
    public $limit = 6;
    public function __construct()
    {
        parent::__construct();
        //$this->connect = new Database(HOST, DATABASE_NAME, USER, PASSWORD);
    }
    public function getCategories(){
//        $this->limit = 6;
        //$offset = ($page - 1) * $this->limit;
        $category = $this->connect->readInformation('id, name, image', 'category');
        return $category;
    }
    public function getListByCategory(){
//        $offset = ($page - 1) * $this->limit;
        $category = $this->connect->readSomeData('id, name, image', 'category', $this->limit, $offset);
        return $category;
    }
//    public function getCategoryCount(){
//        $count = $this->connect->getCount('category');
//        return $count;
//    }
}