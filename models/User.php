<?php
class User extends Model {
    public function __construct()
    {
        parent::__construct();
        //$this->connect = new Database(HOST, DATABASE_NAME, USER, PASSWORD);
    }
    public function registration($username, $email, $password){
        $user = R::dispense('user');
        $user->login = $username;
        $user->email = $email;
        $user->password = md5($password);
        $user->registration_date = date('d M Y');
        R::store($user);
    }
    public function checkUsername($username){
        $pattern_username = '/^[A-zА-я\d][A-zА-я\d]*[_-]?[A-zА-я\d]*[A-zА-я\d]$/i';
        if(!preg_match($pattern_username, $username))
        {
            return true;
        }
        return false;
    }
    public function checkEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return true;
        }
        return false;
    }
    public function checkPassword($password){
//        $pattern_password = '/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/i';
        if(strlen($password) > 6)
        {
            return true;
        }
        return false;
    }
    public function checkUserExists($username)
    {
        if(R::count('user', 'username = ? ', array($username)) > 0)
        {
            return true;
        }
        return false;
    }
    public function checkEmailExists($email)
    {
        if(R::count('user', 'email = ?', array($email)) > 0)
        {
            return true;
        }
        return false;
    }
}