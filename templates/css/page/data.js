/*<![CDATA[*/
function loadCSS(t,e,s){
    "use strict";
    var o=window.document.createElement("link"),n=e||window.document.getElementsByTagName("script")[0];
    o.rel="stylesheet",o.href=t,o.media="only x",n.parentNode.insertBefore(o,n),setTimeout(
        function(){
            o.media=s||"all"
        }
        )
}
loadCSS("//fonts.googleapis.com/css?family=Roboto:400,700|Roboto+Condensed:400,700,400italic,700italic"),$(function(){
    var t=window.location.href;
    $("nav a").each(
        function(){
            this.href===t&&$(this).addClass("active")
        }
        ),$(".searchy").click(
            function(){
                $("#search").slideToggle("slow")
            }
            ),$('.ContactForm').hide(),$(".PopularPosts img").attr("src",function(t,e){
                return e.replace("s72-c","w330-h150-c")
            }
            ), $('.carousel').carousel({interval:6000}) });
/*]]>*/